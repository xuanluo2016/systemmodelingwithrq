from flask import Flask, request, jsonify
from redis import Redis
from rq import Queue
import pickle
import rq_dashboard
import requests
from bs4 import BeautifulSoup
from lxml import html, etree
import requests
import re
# import os
# import sys
# import unicodecsv as csv
# import argparse
# import json
# import pandas as pd
# import numpy as np
# #from lections import Counter

DEBUG = True
SOURCE_URL = 'https://etherscan.io/tx/'

app = Flask(__name__)

# Spawn a client connection to redis server. Here Docker
# provieds a link to our local redis server usinf 'redis'
redisClient = Redis(host='redis')

# Initialize a redis queue instance with name 'bookInfoParser'.
# This name will be used while declaring worker process so that it can
# start processing tasks in it.
#bookInfoParserQueue = Queue('bookInfoParser',connection=redisClient)
txInfoParserQueue = Queue('txInfoParser',connection=redisClient)
#################################
###### Methods ##################
#################################

# generate_redis_key_for_book = lambda bookURL: 'GOODREADS_BOOKS_INFO:' + bookURL
generate_redis_key_for_tx = lambda txURL: 'TX_INFO:' + txURL

# def parse_book_link_for_meta_data(bookLink):
#   htmlString = requests.get(bookLink).content
#   bsTree = BeautifulSoup(htmlString,"html.parser")
#   title = bsTree.find("h1", attrs={"id": "bookTitle"}).string
#   author = bsTree.find("a", attrs={"class": "authorName"}).span.string
#   rating = bsTree.find("span", attrs={"itemprop": "ratingValue"}).string
#   description = ''.join(bsTree.find("div", attrs={"id": "description"}).find("span", attrs={"style": "display:none"}).stripped_strings)
#   return dict(title=title.strip() if title else '',author=author.strip() if author else '',rating=float(rating.strip() if rating else 0),description=description)
#
# def parse_and_persist_book_info(bookUrl):
#   redisKey = generate_redis_key_for_book(bookUrl)
#   bookInfo  = parse_book_link_for_meta_data(bookUrl)
#   redisClient.set(redisKey,pickle.dumps(bookInfo))

def parse_and_persist_tx_info(txUrl):
  redisKey = generate_redis_key_for_tx(txUrl)
  txInfo  = parse_tx_link_for_meta_data(txUrl)
  redisClient.set(redisKey,pickle.dumps(txInfo))

def parse_tx(url):
	response = requests.get(url)
	parser = html.fromstring(response.content)
	tx_result = ''
	tx_detailedTime = ''
	from_address = ''
	to_address = ''

	XPATH_PAGEINFO = './/div[@class = "container row"]'
	pageInfo = parser.xpath(XPATH_PAGEINFO)

	try:
		XPATH_TXID = '//*[@id="tx"]/text()'
		tx_result = parser.xpath(XPATH_TXID)[0]

		for info in pageInfo:
			list = info.xpath('.//a/@href')
			result = list[1]
			from_address = re.sub('/address/','',str(result))

			tx_detailedTime = info.xpath('.//div[8]/text()')[0]

		XPATH_INPUTDATA = '//*[@id="inputdata"]/text()'
		to_address = parser.xpath(XPATH_INPUTDATA)
		
	except Exception as e :
		print("err in parse page")
		print(e.message)

	return (tx_result,tx_detailedTime,from_address,to_address)

def parse_tx_link_for_meta_data(url):
    tx_result = ''
    tx_detailedTime = ''
    from_address = ''
    to_address = ''
    (tx_result, tx_detailedTime, from_address, to_address) = parse_tx(url)
    if(tx_result == ''):
        url = re.sub('0x','',url)
        (tx_result, tx_detailedTime,from_address,to_address) = parse_tx(url)

    return dict(tx_hash = tx_result, tx_time = tx_detailedTime, takerAddr = from_address, makerAddr = to_address)
#################################
#### ENDPOINTS ##################
#################################

# Health check endpoint - responds if service is healthy
@app.route('/')
def health_check():
	return "Web server is up and running!"

#Endpoint that accepts an array of tx hashes for querying tx details
@app.route('/getAllTx', methods=["GET"])
def get_all_tx_info():
  result = []
  txListResult = request.get_json()
  source_url = SOURCE_URL
  if (len(txListResult)):
    for tx_id in txListResult:
      txURL = source_url + str(tx_id)
      redisKey = generate_redis_key_for_tx(txURL)
      cachedValue = redisClient.get(redisKey)
      if cachedValue:
        result.append(pickle.loads(cachedValue))
    return jsonify(result)
  return "Only array of tx hashes is accepted.",400

#Endpoint for retrieving book info from Redis
@app.route('/getTx', methods=["GET"])
def get_tx_info():
  txURL = request.args.get('url', None)
  if (txURL and txURL.startswith('https://www.etherchain.org/tx/')):
    redisKey = generate_redis_key_for_tx(txURL)
    cachedValue = redisClient.get(redisKey)
    if cachedValue:
      return jsonify(pickle.loads(cachedValue))
    return "No meta info found for this tx."
  return "'url' query parameter is required. It must be a valid tx URL.",400

# Endpoint for posting tx ids

@app.route('/parseTx', methods=["POST"])
def parse_tx_urls():
	# get all tx lists
	# filename = './etherscan-data/clean/page1-Z-between-1-2-clean-.csv'
	# txListResult = pd.read_csv(filename)['tx hash']
	txListResult = request.get_json()

	# set absolute url
	source_url = SOURCE_URL
	if (len(txListResult)):
		for tx_id in txListResult:
			txURL = source_url + str(tx_id)
			txInfoParserQueue.enqueue_call(func=parse_and_persist_tx_info,args=(txURL,),job_id=txURL)
		return "%d txs are scheduled for info parsing."%(len(txListResult))
	return "Only json file of tx hash array is accepted.",400

######################################
## Integrating RQ Dashboard with flask
######################################
app.config.from_object(rq_dashboard.default_settings)
app.config.update(REDIS_URL='redis://redis')
app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rqstatus")


if __name__ == '__main__':
	app.run()
