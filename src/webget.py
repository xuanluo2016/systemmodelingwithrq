import requests
import json
import sys
import re
#r = requests.get('https://localhost:5000/parseTx', ["0x3856a6559571cc8434437e04b7c2f085db18a7c733a69645ceb05e1e89972c3e"])

#source_url = 'http://192.168.0.12:5000/'
#source_url = 'http://192.168.0.16:5000/'

source_url = sys.argv[2]
print('example: python webget.py data/request/test.json  http://192.168.0.12:5000/')
r = requests.get(source_url)
print(r.status_code)

with open(sys.argv[1],'r') as infile:
    o = json.load(infile)
    url = source_url + 'getAllTx'
    headers = {'content-type': 'application/json'}
    response = requests.get(url, data=json.dumps(o), headers=headers)
    out = re.sub('request','response',str(sys.argv[1]))
    #print(response.content)
    with open(out, 'w') as outfile:
        #json.dump(response.content, f)
        outfile.write(response.content)
    outfile.close()
infile.close()
