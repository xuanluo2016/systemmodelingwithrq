import requests
import json
import sys
import re
#r = requests.get('https://localhost:5000/parseTx', ["0x3856a6559571cc8434437e04b7c2f085db18a7c733a69645ceb05e1e89972c3e"])

#source_url = 'http://192.168.0.16:5000/'
#source_url = 'http://192.168.0.12:5000/'
source_url = sys.argv[2]
print("example: ")
r = requests.get(source_url)
print(r.status_code)

with open(sys.argv[1],'r') as infile:
    o = json.load(infile)
    chunkSize = 10000
    url = source_url + 'parseTx'
    headers = {'content-type': 'application/json'}
    for i in xrange(0, len(o), chunkSize):
        response = requests.post(url, data=json.dumps(o[i:i+chunkSize]), headers=headers)
        print(response.content)
infile.close()
